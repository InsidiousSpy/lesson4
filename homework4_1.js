/*Написать функцию которая генерирует массив случайных значений, 
таким образом что все элементы результирующего массива являются уникальными. 
Генерациями происходит в рамках чисел от N до M, где N,M - могут быть как 
положительные так и отрицательными, и еще одним параметром количество значений 
которые нужно сгенерировать. Если количество генерируемых значений больше чем 
чисел в диапазоне - отдавать пустой массив.*/

var min=-8;
var max=9;
var quantity = 10;

function getRandom(min, max) {
    return parseInt(Math.random() * (max - min) + min);
  }





function checkUnique(m,elem){
    var status=true;
    for(var j = 0; j < m.length; j++){
        if (elem == m[j]){
            status=false;
            break;
        };
    }
    return status;
}


function genUniqueMassive(min, max, quantity){
var result =[];
var i=0;
if ((max-min+1)>=quantity){
    do {  
        var elem = getRandom(min,max);
        if (checkUnique(result, elem)){
         result.push(elem);
         i++
        }
    } while (i<quantity );

} 
return result;
}
console.log(genUniqueMassive(min, max, quantity));
